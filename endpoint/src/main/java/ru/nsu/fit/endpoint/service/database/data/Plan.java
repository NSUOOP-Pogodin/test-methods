package ru.nsu.fit.endpoint.service.database.data;

import org.apache.commons.lang.Validate;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Plan {
    private UUID id;
    /* Длина не больше 128 символов и не меньше 2 включительно не содержит спец символов */
    private String name;
    /* Длина не больше 1024 символов и не меньше 1 включительно */
    private String details;
    /* Не больше 999999 и не меньше 1 включительно */
    private int maxSeats;
    /* Не больше 999999 и не меньше 1 включительно, minSeats >= maxSeats */
    private int minSeats;
    /* Больше ли равно 0 но меньше либо равно 999999 */
    private int feePerUnit;

    public Plan() {

    }

    public Plan (String name, String details, int maxSeats, int minSeats, int feePerUnit) {
        this.id = UUID.randomUUID();
        this.name = name;
        this.details = details;
        this.maxSeats = maxSeats;
        this.minSeats = minSeats;
        this.feePerUnit = feePerUnit;

        validateName(name);
        validateDetails(details);
        validateMaxSeats(maxSeats);
        validateMinSeats(minSeats, maxSeats);
        validateFeePerUnit(feePerUnit);
    }

    public void validateName(String name) {
        Validate.notNull(name, "Name must be not null.");
        Validate.isTrue(name.length() <= 128, "Name is too long.");
        Validate.isTrue(name.length() >= 2, "Name is too short.");
        for (int i = 0; i < name.length(); i++) {
            Validate.isTrue(Character.isAlphabetic(name.charAt(i)), name.charAt(i) + " is incorrect symbol.");
        }
    }

    public void validateDetails(String details){
        Validate.notNull(details, "Details must be not null.");
        Validate.isTrue(details.length() <= 1024, "Details is very big.");
        Validate.isTrue(details.length() >= 1, "Details is very short.");
    }

    public void validateMaxSeats(int maxSeats){
        Validate.isTrue(maxSeats <= 999999, "MaxSeats is very big.");
        Validate.isTrue(maxSeats >= 1, "MaxSeats is very small.");
    }

    public void validateMinSeats(int minSeats, int maxSeats){

        Validate.isTrue(minSeats <= 999999, "MinSeats is very big.");
        Validate.isTrue(minSeats >= 1, "MinSeats is very small.");
        Validate.isTrue(minSeats <= maxSeats, "MaxSeats must be more than minSeats.");
    }

    public void validateFeePerUnit(int feePerUnit){
        Validate.isTrue(feePerUnit <= 999999, "FeePerUnit is very big.");
        Validate.isTrue(feePerUnit >= 0, "FeePerUnit is very small.");
    }
}
