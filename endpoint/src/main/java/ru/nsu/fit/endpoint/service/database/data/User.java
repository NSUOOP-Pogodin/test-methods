package ru.nsu.fit.endpoint.service.database.data;

import org.apache.commons.lang.Validate;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class User {
    private UUID customerId;
    private UUID[] subscriptionIds;
    private UUID id;
    /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
    private String firstName;
    /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
    private String lastName;
    /* указывается в виде email, проверить email на корректность */
    private String login;
    /* длина от 6 до 12 символов включительно, недолжен быть простым, не должен содержать части login, firstName, lastName */
    private String pass;
    private UserRole userRole;

    public static enum UserRole {
        COMPANY_ADMINISTRATOR("Company administrator"),
        TECHNICAL_ADMINISTRATOR("Technical administrator"),
        BILLING_ADMINISTRATOR("Billing administrator"),
        USER("User");

        private String roleName;

        UserRole(String roleName) {
            this.roleName = roleName;
        }

        public String getRoleName() {
            return roleName;
        }
    }

    public User(String firstName, String lastName, String login, String pass) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.login = login;
        this.pass = pass;

        validateName(firstName, "FirstName");
        validateName(lastName, "LastName");
        validateLogin(login);
        validatePassword(pass, login, firstName, lastName);
    }

    public void validateName(String name, String property) {
        Validate.notNull(name, property + " must be not null.");
        Validate.isTrue(name.length() <= 12, property + " is too long.");
        Validate.isTrue(name.length() >= 2, property + " is too short.");
        Validate.isTrue(!name.contains(" "), property + " must be without space char.");
        Validate.isTrue(name.charAt(0) > 'A' && name.charAt(0) < 'Z', property + " must start with correct symbol.");
        for (int i = 1; i < name.length(); i++) {
            Validate.isTrue(name.charAt(i) > 'a' && name.charAt(i) < 'z', property + "'s " + name.charAt(i) + " is incorrect symbol.");
        }
    }

    public void validateLogin(String login) {
        Validate.notNull(login, "Login must be not null.");
        Validate.isTrue(login.contains("@") && login.contains("."), "Login is invalid.");
    }

    public void validatePassword(String pass, String login, String firstName, String lastName) {
        Validate.notNull(pass, "Password must be not null.");
        Validate.isTrue(pass.length() >= 6 && pass.length() <= 12, "Password's length should be more or equal 6 symbols and less or equal 12 symbols");
        Validate.isTrue(!pass.equalsIgnoreCase("123qwe"), "Password is easy");
        Validate.isTrue(!pass.contains(firstName) && !pass.contains(lastName) && !pass.contains(login.substring(0, login.indexOf('@'))), "Password must not contain firstname, lastname or login.");
    }


}
