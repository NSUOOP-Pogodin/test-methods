package ru.nsu.fit.endpoint.service.databse.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.service.database.data.Customer;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class CustomerTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCreateNewCustomer() {
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "strongpass", 0);
    }

    @Test
    public void testCreateNewCustomerWithShortPass() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Password's length should be more or equal 6 symbols");
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "123", 0);
    }
    @Test
    public void testCreateNewCustomerWithShortPassNorm(){
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "123456", 0);
    }

    @Test
    public void testCreateNewCustomerWithLongPass() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Password's length should be more or equal 6 symbols");
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "123qwe123qwe1", 0);
    }
    @Test
    public void testCreateNewCustomerWithLongPassNorm(){
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "123qwe123qwe", 0);
    }

    @Test
    public void testCreateNewCustomerWithEasyPass() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Password is easy");
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "123qwe", 0);
    }
    @Test
    public void testCreateNewCustomerWithEasyPass2() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Password must not contain firstname, lastname or login.");
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "Wick123", 0);
    }

    @Test
    public void testCreateNewCustomerWithEasyPass3() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Password must not contain firstname, lastname or login.");
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "John123", 0);
    }

    @Test
    public void testCreateNewCustomerWithNullPass() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Password must be not null");
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", null, 0);
    }

    @Test
    public void testCreateNewCustomerWithShortFirstName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("FirstName is too short.");
        new Customer.CustomerData("J", "Wick", "john_wick@gmail.com", "john_wick", 0);
    }

    @Test
    public void testCreateNewCustomerWithShortFirstNameNorm() {
        new Customer.CustomerData("Jo", "Wick", "john_wick@gmail.com", "jon_wck", 0);
    }

    @Test
    public void testCreateNewCustomerWithLongFirstNameNorm() {
        new Customer.CustomerData("Johnjohnjohn", "Wick", "john_wick@gmail.com", "jon_wik", 0);
    }

    @Test
    public void testCreateNewCustomerWithLongFirstName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("FirstName is too long.");
        new Customer.CustomerData("Johnjohnjohnj", "Wick", "john_wick@gmail.com", "john_wick", 0);
    }

    @Test
    public void testCreateNewCustomerWithSpaceInFirstName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("FirstName must be without space char.");
        new Customer.CustomerData("Joh n", "Wick", "john_wick@gmail.com", "john wick", 0);
    }

    @Test
    public void testCreateNewCustomerWithSmallFirstLetterInFirstName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("FirstName must start with correct symbol.");
        new Customer.CustomerData("john", "Wick", "john_wick@gmail.com", "john_wick", 0);
    }

    @Test
    public void testCreateNewCustomerWithBigLettersInFirstName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("FirstName's O is incorrect symbol.");
        new Customer.CustomerData("JOHN", "Wick", "john_wick@gmail.com", "john_wick", 0);
    }

    @Test
    public void testCreateNewCustomerWithShortLastName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("LastName is too short.");
        new Customer.CustomerData("John", "W", "john_wick@gmail.com", "john_wick", 0);
    }

    @Test
    public void testCreateNewCustomerWithShortLastNameNorm() {
        new Customer.CustomerData("John", "Wi", "john_wick@gmail.com", "joen_wiwk", 0);
    }

    @Test
    public void testCreateNewCustomerWithLongLastNameNorm2() {
        new Customer.CustomerData("John", "Wickwickwick", "john_wick@gmail.com", "jodhn_weick", 0);
    }

    @Test
    public void testCreateNewCustomerWithLongLastName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("LastName is too long.");
        new Customer.CustomerData("John", "Wickwickwick1", "john_wick@gmail.com", "johg_wieck", 0);
    }

    @Test
    public void testCreateNewCustomerWithSpaceInLastName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("LastName must be without space char.");
        new Customer.CustomerData("John", "Wic k", "john_wick@gmail.com", "joghn_wigck", 0);
    }

    @Test
    public void testCreateNewCustomerWithSmallFirstLetterInLastname() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("LastName must start with correct symbol.");
        new Customer.CustomerData("John", "wick", "john_wick@gmail.com", "johgn_wigck", 0);
    }

    @Test
    public void testCreateNewCustomerWithBigLettersInLastName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("LastName's I is incorrect symbol.");
        new Customer.CustomerData("John", "WICK", "john_wick@gmail.com", "johgn_wicgk", 0);
    }

    @Test
    public void testCreateNewCustomerIncorrectLogin() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Login is invalid.");
        new Customer.CustomerData("John", "Wick", "john_wickgmail.com", "jon_wck", 0);
    }

    @Test
    public void testCreateNewCustomerIncorrectLogin2() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Login is invalid.");
        new Customer.CustomerData("John", "Wick", "john_wick@gmailcom", "jon_wck", 0);
    }

    @Test
    public void testCreateNewCustomerIncorrectLogin3() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Login must be not null.");
        new Customer.CustomerData("John", "Wick", null, "jon_wck", 0);
    }

    @Test
    public void testCreateNewCustomerNegativeMoney() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Money must be positive value.");
        new Customer.CustomerData("John", "Wick", "suka@blyat.nah", "jon_wck", -1);
    }

    @Test
    public void testCreateNewCustomerNegativeMoneyNorm(){
        new Customer.CustomerData("John", "Wick", "suka@blyat.nah", "jon_wck", 0);
    }
}
