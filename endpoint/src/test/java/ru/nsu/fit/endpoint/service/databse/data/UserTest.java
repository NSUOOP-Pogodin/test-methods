package ru.nsu.fit.endpoint.service.databse.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.service.database.data.User;

/**
 * Created by I on 06.11.2016.
 */
public class UserTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCreateNewCustomer() throws Exception {
        new User("John", "Wick", "john_wick@gmail.com", "strongpass");
    }

    @Test
    public void testCreateNewCustomerWithShortPass() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Password's length should be more or equal 6 symbols and less or equal 12 symbols");
        new User("John", "Wick", "john_wick@gmail.com", "12345");
    }

    @Test
    public void testCreateNewCustomerWithShortPassNorm() {
        new User("John", "Wick", "john_wick@gmail.com", "123456");
    }

    @Test
    public void testCreateNewCustomerWithLongPassNorm() {
        new User("John", "Wick", "john_wick@gmail.com", "123qwe123qwe");
    }

    @Test
    public void testCreateNewCustomerWithLongPass() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Password's length should be more or equal 6 symbols and less or equal 12 symbols");
        new User("John", "Wick", "john_wick@gmail.com", "123qwe123qwe1");
    }

    @Test
    public void testCreateNewCustomerWithNullPass() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Password must be not null.");
        new User("John", "Wick", "john_wick@gmail.com", null);
    }

    @Test
    public void testCreateNewCustomerWithEasyPass() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Password must not contain firstname, lastname or login.");
        new User("John", "Wick", "john_wick@gmail.com", "Johnie");
    }

    @Test
    public void testCreateNewCustomerWithEasyPass2() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Password must not contain firstname, lastname or login.");
        new User("John", "Wick", "john_wick@gmail.com", "Wickie");
    }

    @Test
    public void testCreateNewCustomerWithEasyPass3() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Password must not contain firstname, lastname or login.");
        new User("John", "Wick", "john_wick@gmail.com", "john_wick");
    }

    @Test
    public void testCreateNewCustomerWithShortFirstName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("FirstName is too short.");
        new User("J", "Wick", "john_wick@gmail.com", "john_wick");
    }

    @Test
    public void testCreateNewCustomerWithShortFirstNameNorm() {
        new User("Jo", "Wick", "john_wick@gmail.com", "jon_wck");
    }

    @Test
    public void testCreateNewCustomerWithLongFirstNameNorm2() {
        new User("Johnjohnjohn", "Wick", "john_wick@gmail.com", "jon_wik");
    }

    @Test
    public void testCreateNewCustomerWithLongFirstName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("FirstName is too long.");
        new User("Johnjohnjohnj", "Wick", "john_wick@gmail.com", "john_wick");
    }

    @Test
    public void testCreateNewCustomerWithSpaceInFirstName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("FirstName must be without space char.");
        new User("Joh n", "Wick", "john_wick@gmail.com", "john_wick");
    }

    @Test
    public void testCreateNewCustomerWithSmallFirstLetterInFirstName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("FirstName must start with correct symbol.");
        new User("john", "Wick", "john_wick@gmail.com", "john_wick");
    }

    @Test
    public void testCreateNewCustomerWithBigLettersInFirstName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("FirstName's O is incorrect symbol.");
        new User("JOHN", "Wick", "john_wick@gmail.com", "john_wick");
    }

    @Test
    public void testCreateNewCustomerWithShortLastName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("LastName is too short.");
        new User("John", "W", "john_wick@gmail.com", "john_wick");
    }

    @Test
    public void testCreateNewCustomerWithShortLastNameNorm() {
        new User("John", "Wi", "john_wick@gmail.com", "joen_wiwk");
    }

    @Test
    public void testCreateNewCustomerWithLongLastNameNorm2() {
        new User("John", "Wickwickwick", "john_wick@gmail.com", "jodhn_weick");
    }

    @Test
    public void testCreateNewCustomerWithLongLastName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("LastName is too long.");
        new User("John", "Wickwickwick1", "john_wick@gmail.com", "johg_wieck");
    }

    @Test
    public void testCreateNewCustomerWithSpaceInLastName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("LastName must be without space char.");
        new User("John", "Wic k", "john_wick@gmail.com", "joghn_wigck");
    }

    @Test
    public void testCreateNewCustomerWithSmallFirstLetterInLastName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("LastName must start with correct symbol.");
        new User("John", "wick", "john_wick@gmail.com", "johgn_wigck");
    }

    @Test
    public void testCreateNewCustomerWithBigLettersInLastname() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("LastName's I is incorrect symbol.");
        new User("John", "WICK", "john_wick@gmail.com", "johgn_wicgk");
    }

    @Test
    public void testCreateNewCustomerIncorrectLogin() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Login is invalid.");
        new User("John", "Wick", "john_wickgmail.com", "jon_wck");
    }

    @Test
    public void testCreateNewCustomerIncorrectLogin1() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Login is invalid.");
        new User("John", "Wick", "john_wick@gmailcom", "jon_wck");
    }

    @Test
    public void testCreateNewCustomerIncorrectLogin2() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Login must be not null.");
        new User("John", "Wick", null, "jon_wck");
    }
}
