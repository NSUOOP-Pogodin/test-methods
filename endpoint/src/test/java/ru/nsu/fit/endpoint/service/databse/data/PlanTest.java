package ru.nsu.fit.endpoint.service.databse.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.service.database.data.Plan;

/**
 * Created by I on 06.11.2016.
 */
public class PlanTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCreateNewPlan() {
        new Plan("hello", "detail", 333, 222, 1000);
    }

    @Test
    public void testCreateNewPlanNullName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Name must be not null.");
        new Plan(null, "detail", 333, 222, 1000);
    }

    @Test
    public void testCreateNewPlanShortName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Name is too short");
        new Plan("P", "detail", 333, 222, 1000);
    }

    @Test
    public void testCreateNewPlanShortNameNorm() {
        new Plan("Pl", "detail", 333, 222, 1000);
    }

    @Test
    public void testCreateNewPlanLongNameNorm() {
        new Plan("qwertyuiqwertyuiqwertyuiqwertyuiqwertyuiqwertyuiqwertyuiqwertyuiqwertyuiqwertyuiqwertyuiqwertyuiqwertyuiqwertyuiqwertyuiqwertyui", "detail", 333, 222, 1000);
    }

    @Test
    public void testCreateNewPlanLongName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Name is too long.");
        new Plan("qwertyuiqwertyuiqwertyuiqwertyuiqwertyuiqwertyuiqwertyuiqwertyuiqwertyuiqwertyuiqwertyuiqwertyuiqwertyuiqwertyuiqwertyuiqwertyuik", "detail", 333, 222, 1000);
    }


    @Test
    public void testCreateNewPlanWrongName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("1 is incorrect symbol.");
        new Plan("P111", "detail", 333, 222, 1000);
    }

    @Test
    public void testCreateNewPlanNullDetails() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Details must be not null.");
        new Plan("hello", null, 333, 222, 1000);
    }

    @Test
    public void testCreateNewPlanShortDetails() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Details is very short.");
        new Plan("hello", "", 333, 222, 1000);
    }

    @Test
    public void testCreateNewPlanShortDetails1() {
        new Plan("hello", "a", 333, 222, 1000);
    }

    @Test
    public void testCreateNewPlanSmallMaxSeats() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("MaxSeats is very small.");
        new Plan("hello", "a", 0, 222, 1000);
    }

    @Test
    public void testCreateNewPlanSmallMaxSeatsNorm() throws Exception {
        new Plan("hello", "a", 1, 1, 1000);
    }

    @Test
    public void testCreateNewPlanBigMaxSeatsNorm() throws Exception {
        new Plan("hello", "a", 999999, 1, 1000);
    }

    @Test
    public void testCreateNewPlanBigMaxSeats() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("MaxSeats is very big.");
        new Plan("hello", "a", 1000000, 1, 1000);
    }

    @Test
    public void testCreateNewPlanSmallMinSeats() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("MinSeats is very small.");
        new Plan("hello", "a", 111, 0, 1000);
    }

    @Test
    public void testCreateNewPlanSmallMinSeatsNorm() {
        new Plan("hello", "a", 111, 1, 1000);
    }

    @Test
    public void testCreateNewPlanBigMinSeatsNorm() {
        new Plan("hello", "a", 999999, 999999, 1000);
    }

    @Test
    public void testCreateNewPlanBigMinSeats() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("MinSeats is very big.");
        new Plan("hello", "a", 100000, 1000000, 1000);
    }

    @Test
    public void testCreateNewPlanMinMaxSeats() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("MaxSeats must be more than minSeats.");
        new Plan("hello", "a", 100000, 100001, 1000);
    }

    @Test
    public void testCreateNewPlanSmallFeePerUnit() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("FeePerUnit is very small.");
        new Plan("hello", "detail", 333, 222, -1);
    }

    @Test
    public void testCreateNewPlanSmallFeePerUnitNorm() {
        new Plan("hello", "detail", 333, 222, 0);
    }

    @Test
    public void testCreateNewPlanBigFeePerUnitNorm() {
        new Plan("hello", "detail", 333, 222, 999999);
    }

    @Test
    public void testCreateNewPlanBigFeePerUnit() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("FeePerUnit is very big.");
        new Plan("hello", "detail", 333, 222, 1000000);
    }
}
